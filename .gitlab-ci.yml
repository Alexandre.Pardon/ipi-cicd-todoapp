default:
  # Définit une image Docker à utiliser pour les jobs,
  # si une image n'est pas spécifiée pour un job.
  image: node:lts-alpine
  # Stocke les dépendances installées avec npm en cache,
  # pour les réutiliser dans les jobs suivants.
  cache:
    paths:
      - node_modules/
      - backend/dist/
      - frontend/dist/
      - frontend_image.tar
      - backend_image.tar

# Définit les étapes de pipeline.
# Chaque étape est exécutée dans l'ordre défini.
stages:
  - check_vars
  - install
  - lint_backend
  - lint_frontend
  - test_backend
  - test_frontend
  - build_backend
  - build_frontend
  - test_e2e
  - build_backend_image
  - build_frontend_image
  - push_backend_image
  - push_frontend_image
  - deploy_backend
  - deploy_frontend

variables:
  IMAGE_NAME_PREFIX: "${DOCKER_HUB_USERNAME}/ipi-todoapp"
  IMAGE_NAME_BACKEND: "${IMAGE_NAME_PREFIX}-backend"
  IMAGE_NAME_FRONTEND: "${IMAGE_NAME_PREFIX}-frontend"

before_script:
  - export CLEAN_TAG=`echo $CI_COMMIT_TAG | sed 's/-backend//g' | sed 's/-frontend//g'`
  # À noter : on crée les deux noms d'image (backend et frontend) mais seul celui correspondant
  # à la fin du tag (par ex. v1.0.0-backend) sera utilisé pour le déploiement (ici username/ipi-todoapp-backend:1.0.0).
  - export IMAGE_NAME_TAG_BACKEND="${IMAGE_NAME_BACKEND}:${CLEAN_TAG}"
  - export IMAGE_NAME_TAG_FRONTEND="${IMAGE_NAME_FRONTEND}:${CLEAN_TAG}"

check_vars:
  stage: check_vars
  script:
    - echo "DUMP IMAGE TAG NAMES"
    - echo "IMAGE_NAME_PREFIX = $IMAGE_NAME_PREFIX"
    - echo "IMAGE_NAME_BACKEND = $IMAGE_NAME_BACKEND"
    - echo "IMAGE_NAME_FRONTEND = $IMAGE_NAME_FRONTEND"
    - echo "IMAGE_NAME_TAG_BACKEND = $IMAGE_NAME_TAG_BACKEND"
    - echo "IMAGE_NAME_TAG_FRONTEND = $IMAGE_NAME_TAG_FRONTEND"
    - IS_PROTECTED=false
    - if [ "$CI_COMMIT_REF_PROTECTED" == "true" ]; then IS_PROTECTED=true; fi
    - IS_VERSION_TAG=false
    - BACKEND_REGEX="^v[0-9]+\.[0-9]+\.[0-9]+-backend$"
    - FRONTEND_REGEX="^v[0-9]+\.[0-9]+\.[0-9]+-frontend$"
    - if [[ "$CI_COMMIT_TAG" =~ $BACKEND_REGEX || "$CI_COMMIT_TAG" =~ $FRONTEND_REGEX ]]; then IS_VERSION_TAG=true; fi
    - echo "IS_PROTECTED = $IS_PROTECTED"
    - echo "IS_VERSION_TAG = $IS_VERSION_TAG"
    - CHECK_VARS=false
    - if $IS_PROTECTED && $IS_VERSION_TAG; then CHECK_VARS=true; fi
    - echo "CHECK_VARS = $CHECK_VARS"
    - if $CHECK_VARS && [ -z "$K8S_CERT" ]; then echo "K8S_CERT is not set"; exit 1; fi
    - if $CHECK_VARS && [ -z "$K8S_HOST" ]; then echo "K8S_HOST is not set"; exit 1; fi
    - if $CHECK_VARS && [ -z "$K8S_USER" ]; then echo "K8S_USER is not set"; exit 1; fi
    - if $CHECK_VARS && [ -z "$K8S_TOKEN" ]; then echo "K8S_TOKEN is not set"; exit 1; fi
    - if $CHECK_VARS && [ -z "$K8S_NODE" ]; then echo "K8S_NODE is not set"; exit 1; fi
    - if $CHECK_VARS && [ -z "$DOCKER_HUB_USERNAME" ]; then echo "DOCKER_HUB_USERNAME is not set"; exit 1; fi
    - if $CHECK_VARS && [ -z "$DOCKER_HUB_TOKEN" ]; then echo "DOCKER_HUB_TOKEN is not set"; exit 1; fi
    - export CLEAN_TAG=`echo $CI_COMMIT_TAG | sed 's/-backend//g' | sed 's/-frontend//g'`
    - echo "CLEAN_TAG = $CLEAN_TAG"
    - export IMAGE_NAME_PREFIX="${DOCKER_HUB_USERNAME}/ipi-todoapp"
    - export IMAGE_NAME_BACKEND="${IMAGE_NAME_PREFIX}-backend"
    - export IMAGE_NAME_FRONTEND="${IMAGE_NAME_PREFIX}-frontend"
    - export IMAGE_NAME_TAG_BACKEND="${IMAGE_NAME_BACKEND}:${CLEAN_TAG}"
    - export IMAGE_NAME_TAG_FRONTEND="${IMAGE_NAME_FRONTEND}:${CLEAN_TAG}"

install:
  stage: install
  script:
    - echo "Installing dependencies with npm..."
    - npm install
  # TODO: put back after testing test_e2e
  needs:
    - check_vars

lint_backend:
  stage: lint_backend
  script:
    - echo "Linting backend..."
    - cd backend
    - npm run lint
  needs:
    - install

lint_frontend:
  stage: lint_frontend
  script:
    - echo "Linting frontend..."
    - cd frontend
    - npm run lint
  needs:
    - install

test_backend:
  stage: test_backend
  # Définit le service MySQL
  services:
    - name: mariadb:latest
      alias: mysql
  variables:
    MYSQL_ROOT_PASSWORD: root_password
    MYSQL_DATABASE: app_todolist_test
    MYSQL_USER: app_todolist_test
    MYSQL_PASSWORD: app_todolist_test
    NODE_ENV: test
    DB_HOST: mysql
    DB_NAME: app_todolist_test
    DB_USER: app_todolist_test
    DB_PASS: app_todolist_test
  script:
    - echo "Testing backend..."
    - until nc -z -v -w30 mysql 3306; do echo 'Wait for MySQL...'; sleep 5; done
    - cd backend
    - echo "Running migrations..."
    - npx db-migrate up -e test
    - echo "Running integration tests..."
    - npm test
  needs:
    - install

test_frontend:
  stage: test_frontend
  script:
    - echo "Testing frontend..."
    - cd frontend
    - npm test
  needs:
    - install

build_backend:
  stage: build_backend
  script:
    - echo "Building backend..."
    - cd backend
    - npm run build
  needs:
    - install
    # TODO: put back after testing test_e2e
    # - test_backend

build_frontend:
  stage: build_frontend
  script:
    - echo "Building frontend..."
    - cd frontend
    - npm run build
  needs:
    - install
    # TODO: put back after testing test_e2e
    - build_backend
    # - test_frontend

# Try another way to run e2e without docker
test_e2e:
  # image: node:lts-alpine
  image: benoithubert/chromedriver-node:124-lts-alpine3.19-02
  # we need mysql service
  services:
    - name: mariadb:latest
      alias: mysql
  stage: test_e2e
  variables:
    MYSQL_ROOT_PASSWORD: root_password
    MYSQL_DATABASE: app_todolist_test
    MYSQL_USER: app_todolist_test
    MYSQL_PASSWORD: app_todolist_test
    NODE_ENV: test
    DB_HOST: mysql
    DB_NAME: app_todolist_test
    DB_USER: app_todolist_test
    DB_PASS: app_todolist_test
  script:
    # - apk add --no-cache curl
    - curl https://www.example.com
    # check connectivity to MySQL
    - until nc -z -v -w30 mysql 3306; do echo 'Wait for MySQL...'; sleep 5; done
    - cd backend
    - npx db-migrate up -e test
    - cd ..
    - npx pm2 start ecosystem.config.js
    # check connectivity to backend
    - until nc -z -v -w10 localhost 5173; do echo 'Wait for backend...'; sleep 2; done
    - cat ~/.pm2/logs/backend-error.log
    - cat ~/.pm2/logs/backend-out.log
    # - send request to backend
    - curl http://localhost:5173/api/items
    - npm run wdio
  needs:
    - build_frontend

build_backend_image:
  image: docker:latest
  services:
    - docker:dind
  stage: build_backend_image
  artifacts:
    paths:
      - backend_image.tar
  script:
    - echo "Building backend Docker image..."
    - cd backend
    - docker build -t app_backend:latest .
    - docker save -o ../backend_image.tar app_backend:latest
  needs:
    - build_backend
    - test_backend
    - test_e2e

build_frontend_image:
  image: docker:latest
  services:
    - docker:dind
  stage: build_frontend_image
  artifacts:
    paths:
      - frontend_image.tar
  script:
    - echo "Building frontend Docker image..."
    - cd frontend
    - docker build -t app_frontend:latest .
    - docker save -o ../frontend_image.tar app_frontend:latest
  needs:
    - build_frontend
    - test_frontend
    - test_e2e

push_backend_image:
  stage: push_backend_image
  only:
    - /^v\d+\.\d+\.\d+-backend$/
  image: docker:latest
  services:
    - docker:dind
  script:
    - echo "Deploying ${CI_COMMIT_REF_NAME}@${CI_COMMIT_TAG}"
    - docker load -i backend_image.tar
    - docker tag app_backend:latest $IMAGE_NAME_TAG_BACKEND
    # vers Docker Hub
    - docker login -u $DOCKER_HUB_USERNAME -p $DOCKER_HUB_TOKEN
    - docker push $IMAGE_NAME_TAG_BACKEND
  needs:
    - build_backend_image

push_frontend_image:
  stage: push_frontend_image
  only:
    - /^v\d+\.\d+\.\d+-frontend$/
  image: docker:latest
  services:
    - docker:dind
  script:
    - echo "Deploying ${CI_COMMIT_REF_NAME}@${CI_COMMIT_TAG}"
    - docker load -i frontend_image.tar
    - docker tag app_frontend:latest $IMAGE_NAME_TAG_FRONTEND
    # vers Docker Hub
    - docker login -u $DOCKER_HUB_USERNAME -p $DOCKER_HUB_TOKEN
    - docker push $IMAGE_NAME_TAG_FRONTEND
  needs:
    - build_frontend_image

deploy_backend:
  stage: deploy_backend
  only:
    - /^v\d+\.\d+\.\d+-backend$/ # only run for tags that end with -backend
  image: benoithubert/kubectl-deployer:1.29.3
  script:
    - envsubst < template.kubeconfig.j2 > kubeconfig.yaml
    # Substitute K8S_NODE in k8s-manifests/frontend.yml.j2, k8s-manifests/backend.yml.j2, k8s-manifests/mysql.yml.j2
    - envsubst < k8s-manifests/mysql.yml.j2 > k8s-manifests/mysql.yml
    # In addition, substitute IMAGE_NAME_TAG_BACKEND in k8s-manifests/backend.yml.j2
    - envsubst < k8s-manifests/backend.yml.j2 > k8s-manifests/backend.yml
    # In addition, substitute K8S_NODE _and_ K8S_PORT in k8s-manifests/frontend.yml.j2
    # tell kubectl to use the kubeconfig.yaml file as .kube/config and apply manifests
    - kubectl --kubeconfig=kubeconfig.yaml apply -f k8s-manifests/mysql.yml
    - kubectl --kubeconfig=kubeconfig.yaml apply -f k8s-manifests/backend.yml
  needs:
    - push_backend_image

deploy_frontend:
  stage: deploy_frontend
  only:
    - /^v\d+\.\d+\.\d+-frontend$/ # only run for tags that end with -frontend
  image: benoithubert/kubectl-deployer:1.29.3
  script:
    - envsubst < template.kubeconfig.j2 > kubeconfig.yaml
    # Create a K8S_PORT variable e.g. 8011 from K8S_USER e.g. student11
    # Substitute K8S_NODE and K8S_PORT in k8s-manifests/frontend.yml.j2
    - export K8S_PORT=`echo $K8S_USER | sed 's/student//g' | awk '{print $1+8000}'`
    - envsubst < k8s-manifests/frontend.yml.j2 > k8s-manifests/frontend.yml
    # tell kubectl to use the kubeconfig.yaml file as .kube/config and apply manifests
    - kubectl --kubeconfig=kubeconfig.yaml apply -f k8s-manifests/frontend.yml
  needs:
    - push_frontend_image
