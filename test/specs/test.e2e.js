const { expect, browser, $ } = require('@wdio/globals');

describe('My todo app', () => {
  it('some other test', async () => {
    await browser.url('/');

    const pageTitle = await $('#pageTitle');
    await pageTitle.waitForDisplayed();

    const pageTitleText = await pageTitle.getText();
    await expect(pageTitleText).toBe('Todo App');
  });

  it('should display message from server', async () => {
    await browser.url('/');

    const serverMessage = await $('#serverMessage');
    await serverMessage.waitForDisplayed();

    const serverMessageText = await serverMessage.getText();
    await expect(serverMessageText).toBe('Hello World!');
  });

  it('should add an item', async () => {
    await browser.url('/');

    const itemNameInput = await $('#itemName');
    await itemNameInput.waitForDisplayed();

    const today = new Date();
    const hhmm = today.toTimeString().slice(0, 5);
    const itemName = `Buy milk at ${hhmm}`;
    await itemNameInput.setValue(itemName);
    const submitItemButton = await $('#submitItem');
    const listItems = await $$('.items li');
    const itemsCount = listItems.length;
    await submitItemButton.click();

    let newItems;
    await browser.waitUntil(
      async function () {
        newItems = await $$('.items li');
        return newItems.length === itemsCount + 1;
      },
      {
        timeout: 5000,
        timeoutMsg: 'expected new item to appear within 5s',
      }
    );

    expect(newItems.length).toBe(itemsCount + 1);
    const lastItemSpan = await newItems[itemsCount].$('span');
    const lastItemText = await lastItemSpan.getText();
    expect(lastItemText).toBe(itemName);
  });
});
