import request from 'supertest';
import app from '../../src/app';

describe('GET /message endpoint', () => {
  it('should return a message', async () => {
    const res = await request(app).get('/api/message');
    expect(res.statusCode).toEqual(200);
    expect(res.body.message).toEqual('Hello World!');
  });
});
