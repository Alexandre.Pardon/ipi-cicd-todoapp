import dotenv from 'dotenv';

dotenv.config();

export const port = process.env.PORT || 5000;

export const dbConfig = {
  host: process.env.DB_HOST || 'localhost',
  port: process.env.DB_PORT || 3306,
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: process.env.DB_NAME,
};

const obfuscatePassword = (password, nonHiddenBits) =>
  password.slice(0, nonHiddenBits) +
  '*'.repeat(password.length - nonHiddenBits * 2) +
  password.slice(-nonHiddenBits);

if (process.env.NODE_ENV !== 'test') {
  console.log('>> db config', {
    ...dbConfig,
    password: dbConfig.password && obfuscatePassword(dbConfig.password, 2),
  });
}
