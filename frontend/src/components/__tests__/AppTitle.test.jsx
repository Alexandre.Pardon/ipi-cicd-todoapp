// src/App.test.tsx
import { it, expect } from 'vitest';
import { render, screen } from '@testing-library/react';
import AppTitle from '../AppTitle';

it('renders without crashing', () => {
  render(<AppTitle title="Hello World" />);
  const titleElement = screen.getByText(/hello world/i);
  expect(titleElement).toBeInTheDocument();
});
