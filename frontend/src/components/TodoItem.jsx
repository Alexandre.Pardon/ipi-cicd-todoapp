import { useState } from 'react';
import PropTypes from 'prop-types';

import './TodoItem.css';

export default function TodoItem({ item, handleDeleteItem, handleUpdateItem }) {
  const [isEditing, setIsEditing] = useState(false);
  const [name, setName] = useState(item.name);

  const toggleIsEditing = () => setIsEditing(!isEditing);

  const cancelEditing = () => {
    setName(item.name);
    toggleIsEditing();
  };

  const saveEditing = (e) => {
    e.preventDefault();
    handleUpdateItem(item.id, { name, done: item.done });
    toggleIsEditing();
  };

  const toggleItemDone = () =>
    handleUpdateItem(item.id, { name: item.name, done: !item.done });

  return (
    <li
      key={item.id}
      className={`TodoItem TodoItem-${item.done ? 'done' : 'todo'}`}
    >
      <input type="checkbox" checked={item.done} onChange={toggleItemDone} />
      {isEditing ? (
        <form className="TodoItem-name" onSubmit={saveEditing}>
          <input value={name} onChange={(e) => setName(e.target.value)} />
          <button type="submit">✅</button>
          <button type="button" onClick={cancelEditing}>
            ❌
          </button>
        </form>
      ) : (
        <span className="TodoItem-name">{item.name}</span>
      )}
      <span className="TodoItem-createdAt">{item.createdAt?.slice(0, 16)}</span>

      <button
        type="button"
        className="TodoItem-update-btn"
        onClick={toggleIsEditing}
      >
        Edit
      </button>
      <button
        type="button"
        className="TodoItem-delete-btn"
        onClick={() => handleDeleteItem(item.id)}
      >
        Delete
      </button>
    </li>
  );
}

TodoItem.propTypes = {
  item: PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    done: PropTypes.bool.isRequired,
    createdAt: PropTypes.string.isRequired,
  }),
  handleDeleteItem: PropTypes.func.isRequired,
  handleUpdateItem: PropTypes.func.isRequired,
};
